package com.deloitte.cicd;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CicdServicesApplication {

	public static void main(String[] args) {
		SpringApplication.run(CicdServicesApplication.class, args);
	}
}
