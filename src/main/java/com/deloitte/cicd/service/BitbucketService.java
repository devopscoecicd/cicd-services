package com.deloitte.cicd.service;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

public class BitbucketService {

	public void uploadBulkAccessRequest(InputStream fileInputStream) throws IOException {
		
		Reader reader = new InputStreamReader(fileInputStream);
		CSVParser csvParser = new CSVParser(reader, CSVFormat.DEFAULT.withFirstRecordAsHeader().withIgnoreHeaderCase().withTrim());
	
		Iterable<CSVRecord> csvRecords = csvParser.getRecords();
		
		for(CSVRecord csvRecord :csvRecords){
						System.out.println(csvRecord.get(0));
						System.out.println(csvRecord.get(1));
						System.out.println(csvRecord.get(2));
						
			
			/*testCaseService.saveTestCase(new TestCaseForm(csvRecord.get("Test Case Description"), csvRecord.get("Table Name/Subject Area"),
					csvRecord.get("Source"), csvRecord.get("Target"), csvRecord.get("Query/Filter"), csvRecord.get("Partition/Version End column"),
					csvRecord.get("KEYs"), csvRecord.get("User"), csvRecord.get("Project"), csvRecord.get("Test Case Type"),
					csvRecord.get("Status"), csvRecord.get("Test Case Sub Type")));*/
		}
		
		csvParser.close();
		
		
	}
}
