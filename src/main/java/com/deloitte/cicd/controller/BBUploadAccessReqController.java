package com.deloitte.cicd.controller;

import java.io.IOException;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.deloitte.cicd.service.BitbucketService;

@Controller
public class BBUploadAccessReqController {
	
	private BitbucketService bbServices;
	
	@GetMapping("/uploadBulkAccessRequest")
	public String addAccessRequest(Model model) {
		return "uploadBBBulkAccess";
	}

	@PostMapping(value="/uploadBulkAccessRequest", params="action=upload")
	public String uploadAccessRequest(@RequestParam("myfile")MultipartFile file,Model model) throws IOException
	{
		if(null != file && file.isEmpty()) {
			System.out.println("Invalid file");
			model.addAttribute("title", "Invalid File");
			return "successfulFileUpload";
		}else {
			System.out.println(">>>>>>> uploadAccessRequest ");
			bbServices.uploadBulkAccessRequest(file.getInputStream());
			model.addAttribute("title", "Uploaded File Successfully");
			return "successfulFileUpload";
		}
			
	}
	
}
