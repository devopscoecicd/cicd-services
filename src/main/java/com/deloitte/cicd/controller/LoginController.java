package com.deloitte.cicd.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import com.deloitte.cicd.model.Login;
import com.deloitte.cicd.service.LoginService;

@Controller
public class LoginController {
	@GetMapping("/login")
	public String loginProcess(Model model) {
		model.addAttribute("login", new Login());
		return "login";
	}
	
	@PostMapping("/login")
	public String loginSubmit(@ModelAttribute Login login, Model model) {
		LoginService loginService = new LoginService();
		Boolean result = loginService.validateLogin(login);
		if(result==Boolean.TRUE) {
			//model.addAttribute("loginStatusMessage","Login SuccessFull");
			return "welcome";
		}
		else {
			model.addAttribute("loginStatusMessage","Login unsuccessfull, please provide correct username and password");
			return "login";
		}
				
	}
	

}
