package com.deloitte.cicd.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HelloController {
	@GetMapping("/home")
	String home() {
		return "home";
	}
	
	@GetMapping("/welcome")
	String home1() {
		return "welcome";
	}

}
